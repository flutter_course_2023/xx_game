import 'dart:math';

import 'package:flutter/material.dart';
import 'ui.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: GameScreen(),
    );
  }
}

// class ColoredContainer extends StatefulWidget {
//   const ColoredContainer({Key? key}) : super(key: key);
//   @override
//   State<ColoredContainer> createState() => _ColoredContainerState();
// }
//
// class _ColoredContainerState extends State<ColoredContainer> {
//
//   int r = Random().nextInt(255);
//   int g = Random().nextInt(255);
//   int b = Random().nextInt(255);
//
//   double w = 100;
//   double h = 200;
//
//   @override
//   Widget build(BuildContext context) {
//     r = Random().nextInt(255);
//     g = Random().nextInt(255);
//     b = Random().nextInt(255);
//     return GestureDetector(
//       onTap: _change,
//       child: AnimatedContainer(
//         duration: Duration(
//           milliseconds: 600
//         ),
//         width: w,
//         height: h,
//         color: Color.fromRGBO(r, g, b, 1),
//       ),
//     );
//   }
//
//   void _change(){
//     w = (Random().nextInt(100) + 50).toDouble();
//     h = (Random().nextInt(100) + 100).toDouble();
//     setState(() {});
//   }
// }


