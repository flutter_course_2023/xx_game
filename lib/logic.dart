
class GameLogic{

  List<List<bool>> _grid =[
    [false,false,false,],
    [false,false,false,],
    [false,false,false,],
  ];

  String getIJ(int i,int j) =>
     _grid[i][j] ? 'X' : '';

  void play(int i,int j){
    _grid[i][j] = true;
  }

  bool check(){
    for(int i = 0 ; i < _grid.length ; i ++){
        if(
            _grid[i][0] == _grid[i][1]
            && _grid[i][1] == _grid[i][2]
            && _grid[i][0] == true
        )
          return true;
        if(
        _grid[0][i] == _grid[1][i]
            && _grid[1][i] == _grid[2][i]
            && _grid[0][i] == true
        )
          return true;
    }

    if(
    _grid[0][0] == _grid[1][1]
        && _grid[1][1] == _grid[2][2]
        && _grid[0][0] == true
    )
      return true;

    if(
    _grid[0][2] == _grid[1][1]
        && _grid[1][1] == _grid[2][0]
        && _grid[1][1] == true
    )
      return true;

    return false;
  }

  void replay(){
    _grid =[
      [false,false,false,],
      [false,false,false,],
      [false,false,false,],
    ];
  }
}