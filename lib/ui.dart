import 'package:flutter/material.dart';
import 'package:xx_game/logic.dart';

final GameLogic logic = GameLogic();

class GameScreen extends StatefulWidget {
  const GameScreen({Key? key}) : super(key: key);

  @override
  State<GameScreen> createState() => _GameScreenState();
}

class _GameScreenState extends State<GameScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text("X X"),
        ),
        backgroundColor: Colors.grey[900],
      ),
      backgroundColor: Colors.grey[800],
      body: Column(
        children: [
          for(int i = 0 ; i < 3 ; i ++)
          GameRow(i),
          Expanded(
              flex: 2,
              child: IconButton(
                icon: Icon(
                  Icons.replay,
                  size: 45,
                  color: Colors.cyan,
                ),
                onPressed: ()=> setState(() {
                  logic.replay();
                }),
              )
          )
        ],
      ),
    );
  }
}

class GameRow extends StatelessWidget {
  final int i;
  const GameRow(this.i,{Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 3,
      child: Row(
        children: [
          for(int j = 0 ; j < 3 ; j ++)
          GameContainer(i,j),
        ],
      ),
    );
  }
}

class GameContainer extends StatefulWidget {

  final int i;
  final int j;
  //int value = 0;

   GameContainer(this.i,this.j,{Key? key}) : super(key: key);

  @override
  State<GameContainer> createState() => _GameContainerState();
}

class _GameContainerState extends State<GameContainer> {

  //int value = 0;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: InkWell( // or GestureDetector
        onTap: ()=>_pressed(context),
        child: Container(
          width: double.infinity,
          height: double.infinity,
          margin: EdgeInsets.all(5),
          color: Colors.white.withOpacity(0.5),
          alignment: Alignment.center,
          child: Text(' ${logic.getIJ(widget.i, widget.j)} '),
        ),
      ),
    );
  }

  void _pressed(BuildContext context){
    if(logic.getIJ(widget.i, widget.j).isNotEmpty || logic.check())
      return;
    logic.play(widget.i, widget.j);
    setState(() {});
    if(logic.check()){
      showDialog(
          context: context,
          builder: (context) => Dialog(
            child: Container(
              height: 100,
              width: 250,
              alignment: Alignment.center,
              child: Text('You Win , Game is Done'),
            ),
          )
      );
    }
  }
}

//Random().nextInt(255);